import express, { RequestHandler, response } from 'express';
import { validationResult } from 'express-validator';

const validarCampos = (req: any, res = response, next: any) => {

    const errors = validationResult(req)
    // console.log(errors);
    if (!errors.isEmpty()) {
        return res.json({
            ok: false,
            errors: errors.mapped()
        })
    }
    next()
}

module.exports = { validarCampos }