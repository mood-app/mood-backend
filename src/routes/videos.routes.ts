import { Router } from 'express'
import * as videoCtrl from '../controller/videos.controller'
const router = Router()

router.get('/videos', videoCtrl.obtenerVideos)
router.get('/videos/:id', videoCtrl.obtenerVideo)
router.post('/videos', videoCtrl.crearVideo)
router.delete('/videos/:id', videoCtrl.deleteVideo)
router.put('/videos/:id', videoCtrl.updateVideo)

export default router
