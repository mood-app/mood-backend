import { Router } from 'express'
import { check } from 'express-validator'
import * as userCtrl from '../controller/user.controller'
import validarJWT from '../middlewares/validatJwt'
// import * as  validate from '../middlewares/validarCampos';
const { validarCampos } = require('../middlewares/validarCampos')
const router = Router()

router.post('/registro', [
    //midlewares
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    check('lastName', 'El apellido es obligatorio').not().isEmpty(),
    check('email', 'El email es obligatorio').not().isEmpty(),
    check('email', 'El email no es valido').isEmail(),
    check('password', 'El password es obligatorio').not().isEmpty(),
    check('password', 'El password debe ser de al menos 5 caracteres').isLength({ min: 5 }),
    validarCampos
], userCtrl.registro)


router.post('/login', [
    //midlewares

    check('email', 'El email es obligatorio').not().isEmpty(),
    check('email', 'El email no es valido').isEmail(),
    check('password', 'El password es obligatorio').not().isEmpty(),
    check('password', 'El password debe ser de al menos 5 caracteres').isLength({ min: 5 }),
    validarCampos,

], userCtrl.login)

router.get('/verifyTokenExpire', validarJWT, userCtrl.verificarToken);
export default router